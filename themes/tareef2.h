
/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	"#212d40", /* black */
	"#ff8484", /* red */
	"#7daa92", /* green */
	"#ffcb77", /* yellow */
	"#72a1e5", /* blue */
	"#c8b8db", /* magenta */
	"#44a1a0", /* cyan */
	"#efefef", /* white */

	/* 8 bright colors */
	"#525252", /* black */
	"#ffa3a3", /* red */
	"#9cc5b0", /* green */
	"#ffde96", /* yellow */
	"#91beef", /* blue */
	"#dcd0e8", /* magenta */
	"#5bbebd", /* cyan */
	"#ffffff", /* white */

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"#efefef", /* default foreground colour */
	"black", /* default background colour */

	[259] = "#212d40",
};


/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 258;
unsigned int defaultbg = 259;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
