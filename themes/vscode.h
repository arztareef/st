
/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	"#1f1f1f", /* black */
	"#d75f5f", /* red */
	"#87af87", /* green */
	"#afaf87", /* yellow */
	"#5f87af", /* blue */
	"#af87af", /* magenta */
	"#5f8787", /* cyan */
	"#9e9e9e", /* white */

	/* 8 bright colors */
	"#767676", /* black */
	"#d7875f", /* red */
	"#afd7af", /* green */
	"#d7d787", /* yellow */
	"#87afd7", /* blue */
	"#d7afd7", /* magenta */
	"#87afaf", /* cyan */
	"#bcbcbc", /* white */

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"#efefef", /* default foreground colour */
	"black", /* default background colour */

	[259] = "#1e1e1e",
};


/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 258;
unsigned int defaultbg = 259;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
