
/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	"#282c33", /* black */
	"#e06b74", /* red */
	"#98c379", /* green */
	"#e5c07a", /* yellow */
	"#62aeef", /* blue */
	"#c678dd", /* magenta */
	"#55b6c2", /* cyan */
	"#ABB2BF", /* white */

	/* 8 bright colors */
	"#555555", /* black */
	"#e06b74", /* red */
	"#98c379", /* green */
	"#e5c07a", /* yellow */
	"#62aeef", /* blue */
	"#c678dd", /* magenta */
	"#55b6c2", /* cyan */
	"#cccccc", /* white */

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"#efefef", /* default foreground colour */
	"black", /* default background colour */

	[259] = "#282c33",
};


/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 258;
unsigned int defaultbg = 259;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
