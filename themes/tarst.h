
/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	"#24272c", /* black */
	"#d96c70", /* red */
	"#8abd80", /* green */
	"#d6b676", /* yellow */
	"#6f8fae", /* blue */
	"#c8b8db", /* magenta */
	"#80bda5", /* cyan */
	"#a6a6a6", /* white */

	/* 8 bright colors */
	"#4d4f52", /* black */
	"#e08588", /* red */
	"#a2cb9a", /* green */
	"#e2cb9d", /* yellow */
	"#8da6bf", /* blue */
	"#dcd0e8", /* magenta */
	"#9acbb7", /* cyan */
	"#cccccc", /* white */

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"#efefef", /* default foreground colour */
	"black", /* default background colour */

	[259] = "#24272c",
};


/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 258;
unsigned int defaultbg = 259;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
