
/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	"#282c34", /* black */
	"#e06c75", /* red */
	"#98c379", /* green */
	"#e5c07b", /* yellow */
	"#61afef", /* blue */
	"#c678dd", /* magenta */
	"#56b6c2", /* cyan */
	"#abb2bf", /* white */

	/* 8 bright colors */
	"#5c6370", /* black */
	"#ff7b85", /* red */
	"#b6e991", /* green */
	"#ffd689", /* yellow */
	"#67bbff", /* blue */
	"#e48aff", /* magenta */
	"#67dae8", /* cyan */
	"#abb2bf", /* white */

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"#efefef", /* default foreground colour */
	"black", /* default background colour */

	[259] = "#282c34",
};


/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 258;
unsigned int defaultbg = 259;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
