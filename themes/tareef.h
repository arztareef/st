
/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	"#333c47", /* black */
	"#d85454", /* red */
	"#498a49", /* green */
	"#bf8231", /* yellow */
	"#198bc7", /* blue */
	"#a852a7", /* magenta */
	"#35a9aa", /* cyan */
	"#dfdfdf", /* white */

	/* 8 bright colors */
	"#525252", /* black */
	"#ff8080", /* red */
	"#5ff967", /* green */
	"#f0ae55", /* yellow */
	"#31aced", /* blue */
	"#e974e9", /* magenta */
	"#48d4d6", /* cyan */
	"#ffffff", /* white */

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"gray90", /* default foreground colour */
	"black", /* default background colour */

	[259] = "#333c47",
};


/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 258;
unsigned int defaultbg = 259;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
