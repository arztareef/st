
/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	"#181818", /* black */
	"#ac4242", /* red */
	"#90a959", /* green */
  "#f4bf75", /* yellow */
	"#6a9fb5", /* blue */
	"#a852a7", /* magenta */
	"#75b5aa", /* cyan */
	"#d8d8d8", /* white */

	/* 8 bright colors */
	"#6b6b6b", /* black */
	"#c55555", /* red */
	"#aac474", /* green */
	"#feca88", /* yellow */
	"#82b8c8", /* blue */
	"#c28cb8", /* magenta */
	"#93d3c3", /* cyan */
	"#f8f8f8", /* white */

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc", // cursor
	"#555555",
	"gray90", /* default foreground colour */
	"#181818", /* default background colour */

	[259] = "#181818",
};



/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 258;
unsigned int defaultbg = 259;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
