
/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	"#161b22", /* black */
	"#fa7970", /* red */
	"#7ce38b", /* green */
	"#faa356", /* yellow */
	"#77bdfb", /* blue */
	"#cea5fb", /* magenta */
	"#a2d2fb", /* cyan */
	"#c6cdd5", /* white */

	/* 8 bright colors */
	"#21262d", /* black */
	"#fa7970", /* red */
	"#7ce38b", /* green */
	"#faa356", /* yellow */
	"#77bdfb", /* blue */
	"#cea5fb", /* magenta */
	"#a2d2fb", /* cyan */
	"#ecf2f8", /* white */

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"#efefef", /* default foreground colour */
	"black", /* default background colour */

	[259] = "#0d1117",
};


/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 258;
unsigned int defaultbg = 259;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
